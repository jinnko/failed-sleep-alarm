.PHONY: dependencies
dependencies:
	@if ! command -pv mpg123 >/dev/null; then \
		echo "Failed to find mpg123 in PATH.  Install it first."; \
	fi

.PHONY: install
install: dependencies
	@if [ $(USER) != "root" ]; then echo "make install must be run as root"; exit 1; fi
	install --compare -m 0755 detect-failed-sleep.sh /lib/systemd/system-sleep/10-detect-failed-sleep.sh
	install --compare -m 0755 -d /usr/share/failed-sleep-alarm/
	install --compare -m 0644 pig.mp3 /usr/share/failed-sleep-alarm/
