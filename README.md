# Failed sleep alarm

This tool hooks into the systemd post (suspend,sleep,hibernate) events and checks whether the
action failed in the last two minutes by searching the journal.  If a failure was found it will
unmute the Master alsa channel, set the volume to 35%, then play an audible sound.

## Quick start

1. Check the script to make sure you're happy with it
2. `sudo make install`
3. Suspend & resume, and hope you don't hear the farm animal.

## Dependencies

- amixer
- mpg123

## Sound

The sound of a pig snorting twice recorded at a farm. thanks Caroline Ford for this sound.

License: Attribution 3.0
Recorded by: Caroline Ford
