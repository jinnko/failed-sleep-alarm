#!/usr/bin/env bash

set -euo pipefail

logger -s -p kern.debug "Invoked $(realpath "$0") $*"

case $1/$2 in
  post/*)
    if /usr/bin/journalctl --since "2m ago" | grep -E "Failed to start (Hibernate|Sleep|Suspend)"; then
      amixer -q sset Master unmute
      amixer sset Master '35%'
      for i in {1..3}; do
        mpg123 -q /usr/share/failed-sleep-alarm/pig.mp3
      done
    fi
    ;;
esac
